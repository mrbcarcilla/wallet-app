# Wallet App

Wallet App is a sample project containing two applications; (1) a server application written in Java and (2) a frontend application using AngularJS.

This project features two major functionalities, Top up and Transfer credits.

## Usage

### Running database

To run the postgresql database, use this command from the root folder of the app

```
docker-compose up -d postgres
```

### Running the server
To run the java server, go to the server folder and run Server.java. 

### Running the interfacing app
To run the angular app, go to the web folder and run 

```
ng-serve
```

After running all of these, you can visit the application [here](http://localhost:4200).

## Demo Video
I've also created a demo video [here](https://drive.google.com/open?id=1mr39-mdXfV765T-fwPSJxTRhgVleL6_p) to view a summary of the application.