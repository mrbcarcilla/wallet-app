import { Injectable } from '@angular/core';
import { Wallet } from './wallet';
import { TopUpTransaction } from '../transactions/top-up-transaction';
import { TransferTransaction } from '../transactions/transfer-transaction';

@Injectable()
export class WalletService {
  constructor() { }

  list() {
    return Wallet.list();
  }

  get(id: String) {
    return Wallet.get(id);
  }

  create(wallet: Wallet) {
    return wallet.create();
  }

  topUp(transaction: TopUpTransaction) {
    return Wallet.topUp(transaction);
  }

  transfer(transaction: TransferTransaction) {
    return Wallet.transfer(transaction);
  }
}
