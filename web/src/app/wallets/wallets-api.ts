import { Apollo } from "apollo-angular";
import gql from 'graphql-tag';
import { Wallet } from "./wallet";
import { TopUpTransaction } from "../transactions/top-up-transaction";
import { TransferTransaction } from "../transactions/transfer-transaction";

export class WalletsApi {
  constructor(private apollo: Apollo) {}

  list() {
    const query = gql`
      query listWallets(
        $max: Int,
        $offset: Int,
        $sort: String
      ) {
        listWallets(
          opts: {
            max: $max,
            offset: $offset,
            sort: $sort
          }
        ) {
          total
          wallets {
            id
            created
            updated
            ownerName
            walletId
            balance
          }
        }
      }
    `;

    return this.apollo
      .query({
        variables: {
          offset: 0,
          sort: 'name:asc',
        },
        query
      }).toPromise();
  }

  get(id: String) {
    const query = gql`
      query getWallet(
        $id: ID!
      ) {
        getWallet(
          id: $id
        ) {
          id
          created
          updated
          ownerName
          walletId
          balance
          transactions {
            ... on TopUpTransaction {
              id
              created
              updated
              amount
              from {
                ... on Wallet { 
                  id
                  created
                  updated
                  ownerName
                  walletId
                  balance
                }
              }
              to {
                ... on Wallet { 
                  id
                  created
                  updated
                  ownerName
                  walletId
                  balance
                }
              }
            }
            ... on TransferTransaction {
              id
              created
              updated
              amount
              from {
                ... on Wallet { 
                  id
                  created
                  updated
                  ownerName
                  walletId
                  balance
                }
              }
              to {
                ... on Wallet { 
                  id
                  created
                  updated
                  ownerName
                  walletId
                  balance
                }
              }
            }
          }
        }
      }
    `;

    return this.apollo
      .query({
        variables: {
          id: id
        },
        query
      }).toPromise();
  }

  create(wallet: Wallet) {
    const mutation = gql`
      mutation createWallet(
        $ownerName: String!
      ) {
        createWallet(
          ownerName: $ownerName
        ) {
          id
        }
      }
    `;

    return this.apollo
      .mutate({
        variables: {
          ownerName: wallet.getOwnerName(),
        },
        mutation
      }).toPromise();
  }

  topUp(trx: TopUpTransaction) {
    const mutation = gql`
      mutation topUp(
        $to: ID!,
        $amount: String
      ) {
        topUp(
          to: $to,
          amount: $amount
        ) {
          id
        }
      }
    `;

    return this.apollo
      .mutate({
        variables: {
          to: trx.getTo(),
          amount: trx.getAmount()
        },
        mutation
      }).toPromise();
  }

  transfer(trx: TransferTransaction) {
    const mutation = gql`
      mutation transfer(
        $to: ID!,
        $from: ID!
        $amount: String
      ) {
        transfer(
          to: $to,
          from: $from,
          amount: $amount
        ) {
          id
        }
      }
    `;

    return this.apollo
      .mutate({
        variables: {
          to: trx.getTo(),
          from: trx.getFrom(),
          amount: trx.getAmount()
        },
        mutation
      }).toPromise();
  }
}
