import { Model } from "../model";
import { TopUpTransaction } from "../transactions/top-up-transaction";
import { TransferTransaction } from "../transactions/transfer-transaction";

export class Wallet extends Model {
  private walletId: String
  private ownerName: String
  private balance: BigInteger
  private transactions: []

  public getWalletId(): String {
    return this.walletId;
  }

  public getOwnerName(): String {
    return this.ownerName;
  }

  public withOwnerName(arg: String): this {
    this.ownerName = arg;
    return this;
  }

  public getId(): String {
    return this.getId();
  }

  public getBalance(): BigInteger {
    return this.balance;
  }

  create() {
    return this.api().wallets().create(this);
  }

  static list() {
    return this.api().wallets().list();
  }

  static get(id: String) {
    return this.api().wallets().get(id);
  }

  static topUp(transaction: TopUpTransaction) {
    return this.api().wallets().topUp(transaction);
  }

  static transfer(transaction: TransferTransaction) {
    return this.api().wallets().transfer(transaction);
  }
}

export interface WalletModel {
  id: String;
  walletId: String
  ownerName: String
  balance: BigInteger
  transactions: []
}