import { Apollo } from "apollo-angular";
import { WalletsApi } from "../wallets/wallets-api";

export class ApiClient {
  private readonly api;
  private readonly walletsApi;

  constructor(private apollo: Apollo) {
    this.api = apollo;

    this.walletsApi = new WalletsApi(this.api);
  }

  wallets(): WalletsApi { return this.walletsApi; }
}
