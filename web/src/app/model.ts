import { ApiClient } from "./api/api-client";

export class Model {
  private static _api;

  private id: String;
  private created: Date;
  private updated: Date;

  static setApiClient(client: ApiClient) {
    this._api = client;
  }

  static api(): ApiClient {
    return this._api;
  }

  api(): ApiClient {
    return Model._api;
  }

  public getId(): String {
    return this.id;
  }
}
