import { Component, OnInit } from '@angular/core';
import { WalletService } from '../../wallets/wallet.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl } from '@angular/forms';
import { TransferTransaction } from '../../transactions/transfer-transaction';

@Component({
  selector: 'app-wallet-transfer',
  templateUrl: './wallet-transfer.component.html',
  styleUrls: ['./wallet-transfer.component.css']
})
export class WalletTransferComponent implements OnInit {
  private wallet;
  private form: FormGroup;

  constructor(
    private walletService: WalletService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe(params => {
      this.walletService.get(params.id).then(
        (data: any) => {
          this.wallet = data.data.getWallet;
          console.log("this.wallet " + JSON.stringify(this.wallet));
        },
        error => {
          console.log("Error!");
        }
      );
    });

    this.form =  new FormGroup ({
      to: new FormControl(),
      amount: new FormControl()
    });
  }

  cancel() {
    this.router.navigateByUrl(`/wallets/${this.wallet.id}`);
  }

  transfer() {
    if(Number(this.form.get('amount').value) > Number(this.wallet.balance))
      alert('Invalid amount!');
    else {
      this.walletService.transfer(
        new TransferTransaction()
          .withFrom(this.wallet.walletId)
          .withTo(this.form.get('to').value)
          .withAmount(this.form.get('amount').value)
      ).then(() => this.router.navigateByUrl(`/wallets/${this.wallet.id}`))
      .catch(() => console.log("Error!"));
    }
  }

}
