import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { WalletService } from '../../wallets/wallet.service';
import { TopUpTransaction } from '../../transactions/top-up-transaction';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-wallet-top-up',
  templateUrl: './wallet-top-up.component.html',
  styleUrls: ['./wallet-top-up.component.css']
})
export class WalletTopUpComponent implements OnInit {
  private form: FormGroup;
  private id: String;
  private wallet;

  constructor(
    private walletService: WalletService,
    private activatedRoute: ActivatedRoute,
    private router: Router
    ) { 
      this.form = new FormGroup ({
        amount: new FormControl()
      });
      this.activatedRoute.params.subscribe(params => {
        this.id = params.id;
      });

      this.walletService.get(this.id).then(
        (data: any) => {
          this.wallet = data.data.getWallet;
          console.log("this.wallet " + JSON.stringify(this.wallet));
        },
        error => {
          console.log("Error!");
        }
      );

  }

  topUp() {
    console.log("this.form.get('amount').value " + this.form.get('amount').value);
    this.walletService.topUp(
      new TopUpTransaction()
        .withTo(this.wallet.walletId)
        .withAmount(this.form.get('amount').value)
    )
    .then(() => this.router.navigateByUrl(`/wallets/${this.id}`))
    .catch(() => console.log("Error!"));
  }
  
  cancel() {
    this.router.navigateByUrl(`/wallets/${this.id}`);
  }

  ngOnInit() {
    
  }

}
