import { Component, OnInit } from '@angular/core';
import { WalletService } from '../../wallets/wallet.service';
import { Router } from '@angular/router';
import { initDomAdapter } from '@angular/platform-browser/src/browser';

@Component({
  selector: 'app-wallets',
  templateUrl: './wallets.component.html',
  styleUrls: ['./wallets.component.css']
})
export class WalletsComponent implements OnInit {
  public list = [];

  constructor(
    private walletService: WalletService,
    private router: Router
  ) { }

  ngOnInit() {
    this.init();
  }

  ngAfterViewInit() {
    console.log("here")
    this.init();
  }

  init() {
    this.walletService.list().then(
      (data: any) => {
        console.log("data.data.listItems.items " + JSON.stringify(data.data.listWallets.wallets));  
        this.list = data.data.listWallets.wallets;
      },
      error => {
        alert('An error has occurred');
      }
    );
  }

  view(id: String) {
    this.router.navigateByUrl(`/wallets/${id}`);
  }

  create() {
    this.router.navigateByUrl('/wallets/create');
  }

}
