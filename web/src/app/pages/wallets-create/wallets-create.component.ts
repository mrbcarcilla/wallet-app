import { Component, OnInit } from '@angular/core';
import { WalletService } from '../../wallets/wallet.service';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Wallet } from '../../wallets/wallet';

@Component({
  selector: 'app-wallets-create',
  templateUrl: './wallets-create.component.html',
  styleUrls: ['./wallets-create.component.css']
})
export class WalletsCreateComponent implements OnInit {
  private form: FormGroup

  constructor(
    private walletService: WalletService,
    private router: Router
  ) { 
    this.form = new FormGroup ({
      ownerName: new FormControl()
    });
  }

  ngOnInit() {
  }

  create() {
    this.walletService.create(
      new Wallet().withOwnerName(this.form.get('ownerName').value)
    )
    .then(() => this.router.navigateByUrl('/wallets'))
    .catch(() => console.log("Error!"));
  }

  cancel() {
    this.router.navigateByUrl('/wallets');
  }

}
