import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  private form: FormGroup;
  constructor(private router: Router) { }

  ngOnInit() {
    this.form = new FormGroup ({
      username: new FormControl(),
      password: new FormControl(),
    });
  }

  login() {
    console.log("logging in...");
    if (this.form.get('username').value === 'wallet' && this.form.get('password').value === 'wallet@123') {
      this.router.navigateByUrl('/wallets');

      
    } else {
      alert('Invalid Username/Password');
    }
  }

}
