import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WalletsViewComponent } from './wallets-view.component';

describe('WalletsViewComponent', () => {
  let component: WalletsViewComponent;
  let fixture: ComponentFixture<WalletsViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WalletsViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WalletsViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
