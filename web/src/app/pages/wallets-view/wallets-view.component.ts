import { Component, OnInit } from '@angular/core';
import { WalletService } from '../../wallets/wallet.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Wallet } from '../../wallets/wallet';

@Component({
  selector: 'app-wallets-view',
  templateUrl: './wallets-view.component.html',
  styleUrls: ['./wallets-view.component.css']
})
export class WalletsViewComponent implements OnInit {
  private wallet;

  constructor(
    private walletService: WalletService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit() {
    this.init();
  }

  init() {
    this.activatedRoute.params.subscribe(params => {
      this.walletService.get(params.id).then(
        (data: any) => {
          this.wallet = data.data.getWallet;
          console.log("this.wallet " + JSON.stringify(this.wallet));
        },
        error => {
          console.log("Error!");
        }
      );
    });
  }
  
  
  topUp() {
    this.router.navigate(['/wallets/top-up', {
      id: this.wallet.id
    }]);
  }

  transfer() {
    this.router.navigate(['/wallets/transfer', {
      id: this.wallet.id
    }]);
  }

  back() {
    this.router.navigateByUrl('/wallets');
  }

}
