import { Injectable } from '@angular/core';
import { Apollo } from 'apollo-angular';
import { Model } from './model';
import { ApiClient } from './api/api-client';
import { platform } from 'os';

@Injectable()
export class AppInitializerService {
  constructor(apollo: Apollo) {
    Model.setApiClient(new ApiClient(apollo));
  }


}
