import { Wallet } from "../wallets/wallet";

export class TransferTransaction {
  private from: String;
  private to: String;
  private amount: String;

  public withTo(arg: String): this {
    this.to = arg;
    return this;
  }

  public withFrom(arg: String): this {
    this.from = arg;
    return this;
  }

  public withAmount(arg: String): this {
    this.amount = arg;
    return this;
  }

  public getFrom(): String {
    return this.from;
  }

  public getTo(): String {
    return this.to;
  }

  public getAmount(): String {
    return this.amount;
  }
}

export interface TransferTransactionModel {
  from: Wallet;
  to: Wallet;
  amount: BigInteger;
}