import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './pages/login/login.component';
import { WalletsComponent } from './pages/wallets/wallets.component';
import { WalletsViewComponent } from './pages/wallets-view/wallets-view.component';
import { WalletsCreateComponent } from './pages/wallets-create/wallets-create.component';
import { WalletTopUpComponent } from './pages/wallet-top-up/wallet-top-up.component';
import { WalletTransferComponent } from './pages/wallet-transfer/wallet-transfer.component';

const route: Routes = [
  {
    path: 'login',
    component: LoginComponent,
    data: { title: 'Login' }
  },
  {
    path: '',
    redirectTo: '/login',
    pathMatch: 'full'
  },
  {
    path: 'wallets',
    component: WalletsComponent,
    data: { title: 'Wallets' }
  },
  {
    path: 'wallets/create',
    component: WalletsCreateComponent,
    data: { title: 'Wallets Create' },
    pathMatch: 'full'
  },
  {
    path: 'wallets/top-up',
    component: WalletTopUpComponent,
    data: { title: 'Wallet Top Up' }
  },
  {
    path: 'wallets/transfer',
    component: WalletTransferComponent,
    data: { title: 'Wallet Transfer' }
  },
  {
    path: 'wallets/:id',
    component: WalletsViewComponent,
    data: { title: 'Wallets View' }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(route), 
  ],
  exports: [RouterModule],
  declarations: []
})
export class AppRoutingModule { }
