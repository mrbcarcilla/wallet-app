import { BrowserModule } from '@angular/platform-browser';
import { NgModule, APP_INITIALIZER, Injector } from '@angular/core';

import { AppComponent } from './app.component';
import { LoginComponent } from './pages/login/login.component';
import { AppRoutingModule } from './/app-routing.module';
import { WalletsComponent } from './pages/wallets/wallets.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ApolloModule, APOLLO_OPTIONS, Apollo } from "apollo-angular";
import { HttpLinkModule, HttpLink } from "apollo-angular-link-http";
import { InMemoryCache } from "apollo-cache-inmemory";
import { AppInitializerService } from './app-initializer.service';
import { HttpClientModule } from '@angular/common/http';
import { WalletService } from './wallets/wallet.service';
import { WalletsViewComponent } from './pages/wallets-view/wallets-view.component';
import { WalletsCreateComponent } from './pages/wallets-create/wallets-create.component';
import { WalletTopUpComponent } from './pages/wallet-top-up/wallet-top-up.component';
import { WalletTransferComponent } from './pages/wallet-transfer/wallet-transfer.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    WalletsComponent,
    WalletsViewComponent,
    WalletsCreateComponent,
    WalletTopUpComponent,
    WalletTransferComponent
  ],
  imports: [
    BrowserModule,  
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    ApolloModule,
    HttpLinkModule,
    HttpClientModule
  ],
  providers: [
  WalletService,
  // {
  //   provide: APOLLO_OPTIONS,
  //   useFactory: (httpLink: HttpLink) => {
  //     return {
  //       cache: new InMemoryCache(),
  //       link: httpLink.create({
  //         uri: "http://localhost:8080/server"
  //       })
  //     }
  //   },
  //   deps: [HttpLink]  
  // },
  Apollo,
  {
    provide: APP_INITIALIZER,
    useFactory: () => function() {
      return new AppInitializerService(
        AppModule.injector.get(Apollo)
      )
    },
    multi: true
  }
  ],
  bootstrap: [AppComponent]
})

export class AppModule {
  static injector: Injector;

  constructor(injector: Injector, httpLink: HttpLink, apollo: Apollo) {
    AppModule.injector = injector;

    const link = httpLink.create({
      uri: 'http://localhost:8080/server'
    });

    apollo.create({
      link,
      cache: new InMemoryCache(),
      defaultOptions: {
        watchQuery: {
          fetchPolicy: 'no-cache',
          errorPolicy: 'ignore',
        },
        query: {
          fetchPolicy: 'no-cache',
          errorPolicy: 'all',
        },
      }
    });
  }
}