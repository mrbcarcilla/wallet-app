package com.wallet.app;

import java.time.*;
import java.time.format.*;

public class ServerUtil {
  public static String format(LocalDateTime date) {
    return date.format(DateTimeFormatter.ofPattern(Model.DATE_FORMATTER));
  }

  public static LocalDateTime utcNow() {
    return OffsetDateTime.now(ZoneId.of("UTC")).toLocalDateTime();
  }
}
