package com.wallet.app;

import com.wallet.app.jpa.*;
import org.hibernate.*;

import javax.persistence.*;
import java.time.*;
import java.util.*;

@MappedSuperclass
public abstract class Model {
  public static final String DATE_FORMATTER= "yyyy-MM-dd HH:mm:ss.SSS";

  @Id
  protected String id = UUID.randomUUID().toString();

  @Column(nullable = false)
  private LocalDateTime created;

  @Column(nullable = false)
  private LocalDateTime updated;

  @PrePersist
  public void beforeInsert() {
    created = ServerUtil.utcNow();
    updated = ServerUtil.utcNow();
  }

  public String getId() {
    return id;
  }

  public String getCreated() {
    return ServerUtil.format(created);
  }

  public String getUpdated() {
    return ServerUtil.format(updated);
  }

  public Model save() {
    JPAUtil.getEntityManager()
      .unwrap(Session.class)
      .saveOrUpdate(this);

    return this;
  }
}