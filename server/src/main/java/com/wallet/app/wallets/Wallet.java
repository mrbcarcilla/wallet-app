package com.wallet.app.wallets;

import com.wallet.app.*;
import com.wallet.app.jpa.*;
import com.wallet.app.util.*;
import com.wallet.app.transactions.*;
import org.apache.commons.lang.*;

import javax.persistence.*;
import java.math.*;
import java.util.*;

@Entity
public class Wallet extends Model {
  private static final Repository<Wallet> repo = new Repository(Wallet.class);
  private static final int SHORT_ID_LENGTH = 8;

  @Column
  private String walletId;

  @Column
  private String ownerName;

  @Column
  private BigDecimal balance;

  @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
  List<Transaction> transactions = new ArrayList<Transaction>();

  @Override
  @PrePersist
  public void beforeInsert() {
    setWalletId(RandomStringUtils.randomNumeric(SHORT_ID_LENGTH));
    setBalance(BigDecimal.ZERO);
    super.beforeInsert();
  }

  public String getOwnerName() {
    return ownerName;
  }

  public void setOwnerName(String arg) {
    this.ownerName = arg;
  }

  public Wallet withOwnerName(String arg) {
    setOwnerName(arg);
    return this;
  }

  public String getWalletId() {
    return walletId;
  }

  public void setWalletId(String arg) {
    this.walletId = arg;
  }

  public String getAmount() {
    return balance.toString();
  }

  public BigDecimal getRawBalance() {
    return balance;
  }

  public void setBalance(BigDecimal arg) {
    this.balance = arg;
  }

  public Wallet withBalance(BigDecimal arg) {
    setBalance(arg);
    return this;
  }

  public void transact(Transaction arg) {

    if(arg instanceof TopUpTransaction)
      setBalance(getRawBalance().add(arg.getRawAmount()));
    else {
      setBalance(getRawBalance().add(arg.getRawAmount()));

      Wallet from = arg.getFrom();

      from.addTransaction(arg);
      from.setBalance(from.getRawBalance().subtract(arg.getRawAmount()));
      from.save();
    }

    addTransaction(arg);
  }

  public void addTransaction(Transaction arg) {
    this.transactions.add(arg);
  }

  public static PagedList<Wallet> list(ListOpts opts) {
    return repo.list(opts);
  }

  public static Optional<Wallet> get(String id) {
    return repo.get(id);
  }

  public static Optional<Wallet> getByWalletId(String id) {
    return repo.get((crit, path) -> crit.equal(path.get("walletId"), id));
  }
}
