package com.wallet.app.wallets;

public enum Status {
  IN_STORAGE,
  RESERVED
}
