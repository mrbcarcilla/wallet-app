package com.wallet.app.wallets;

import com.wallet.app.jpa.*;
import com.wallet.app.util.*;

import java.util.*;
import java.util.function.*;

@org.springframework.stereotype.Service
public class WalletService {

  public <T> T transaction(Supplier<T> closure) {
    return JPAUtil.transaction(closure::get);
  }

  public void transaction(Runnable closure) {
    JPAUtil.transaction(closure);
  }

  public Wallet create(Wallet wallet) {
    return (Wallet) transaction(wallet::save);
  }

  public Wallet update(Wallet wallet) {
    return (Wallet) transaction(wallet::save);
  }

  public PagedList<Wallet> list(ListOpts opts) {
    return Wallet.list(opts);
  }

  public Optional<Wallet> get(String id) {
    return Wallet.get(id);
  }

  public Optional<Wallet> getByWalletId(String id) {
    return Wallet.getByWalletId(id);
  }
}


