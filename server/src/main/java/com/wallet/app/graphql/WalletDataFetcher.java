package com.wallet.app.graphql;

import com.wallet.app.transactions.*;
import com.wallet.app.wallets.*;
import com.wallet.app.util.*;
import graphql.schema.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.*;

import java.math.*;
import java.util.*;

@Component
public class WalletDataFetcher {
  @Autowired
  private WalletService walletService;

  public DataFetcher create() {
    return dataFetchingEnvironment -> {
      Map<String, Object> params = dataFetchingEnvironment.getArguments();

      return walletService.create(
        new Wallet().withOwnerName(params.get("ownerName").toString())
      );
    };
  }

  public DataFetcher get() {
    return dataFetchingEnvironment ->
      walletService.get(dataFetchingEnvironment.getArgument("id"))
      .orElse(null);
  }

  public DataFetcher list() {
    return dataFetchingEnvironment ->
      walletService.list(ListOpts.from(dataFetchingEnvironment.getArgument("opts")));
  }

  public DataFetcher topUp() {
    return dataFetchingEnvironment -> {
      Map<String, Object> params = dataFetchingEnvironment.getArguments();

      return walletService.getByWalletId(dataFetchingEnvironment.getArgument("to")).map(wallet -> {
        wallet.transact(
          new TopUpTransaction()
            .withTo(wallet)
            .withAmount(new BigDecimal(params.get("amount").toString()))
        );

        return walletService.update(wallet);
      }).orElseThrow(Exception::new);
    };
  }

  public DataFetcher transfer() {
    return dataFetchingEnvironment -> {
      Map<String, Object> params = dataFetchingEnvironment.getArguments();

      return walletService.getByWalletId(dataFetchingEnvironment.getArgument("to")).map(wallet -> {
        wallet.transact(
          new TransferTransaction()
            .withFrom(walletService.getByWalletId(params.get("from").toString()).get())
            .withTo(wallet)
            .withAmount(new BigDecimal(params.get("amount").toString()))
        );

        return walletService.update(wallet);
      }).orElseThrow(Exception::new);
    };
  }
}
