package com.wallet.app.graphql;

import com.google.common.io.Resources;
import com.wallet.app.transactions.*;
import com.wallet.app.wallets.*;
import com.wallet.app.util.*;
import graphql.*;
import graphql.schema.*;
import graphql.schema.idl.*;
import org.apache.commons.compress.utils.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.context.annotation.*;
import org.springframework.stereotype.*;

import javax.annotation.*;
import java.io.*;
import java.net.*;

import static graphql.schema.idl.TypeRuntimeWiring.*;

@Component
public class GraphQLProvider {
  private GraphQL graphQL;

  @Bean
  public GraphQL graphQL() {
    return graphQL;
  }

  @Autowired
  WalletDataFetcher walletDataFetcher;


  @PostConstruct
  public void init() throws IOException {
    URL url = Resources.getResource("schema.graphql");
    String sdl = Resources.toString(url, Charsets.UTF_8);
    GraphQLSchema graphQLSchema = buildSchema(sdl);
    this.graphQL = GraphQL.newGraphQL(graphQLSchema).build();
  }

  private GraphQLSchema buildSchema(String sdl) {
    TypeDefinitionRegistry typeRegistry = new SchemaParser().parse(sdl);
    RuntimeWiring runtimeWiring = buildWiring();
    SchemaGenerator schemaGenerator = new SchemaGenerator();
    return schemaGenerator.makeExecutableSchema(typeRegistry, runtimeWiring);
  }

  private RuntimeWiring buildWiring() {
    return RuntimeWiring.newRuntimeWiring()
      .type(newTypeWiring("Query")
        .dataFetcher("listWallets", walletDataFetcher.list())
        .dataFetcher("getWallet", walletDataFetcher.get())
      )
      .type(newTypeWiring("Mutation")
        .dataFetcher("createWallet", walletDataFetcher.create())
        .dataFetcher("topUp", walletDataFetcher.topUp())
        .dataFetcher("transfer", walletDataFetcher.transfer())
      )
      .type("PagedList", typeWriting -> typeWriting.typeResolver(pagedListResolver))
      .type("Model", typeWriting -> typeWriting.typeResolver(transactionResolver))
      .type("Transaction", typeWriting -> typeWriting.typeResolver(modelResolver))
      .build();
  }

  TypeResolver pagedListResolver = env -> {
    Class t = ((PagedList) env.getObject()).getType();

    if(t == Wallet.class)
      return env.getSchema().getObjectType("WalletsPagedList");

    else
      return env.getSchema().getObjectType("Model");
  };

  TypeResolver transactionResolver = env -> {
    Object obj = env.getObject();

    if(obj instanceof TopUpTransaction)
      return env.getSchema().getObjectType("TopUpTransaction");
    else
      return env.getSchema().getObjectType("TransferTransaction");
  };

  TypeResolver modelResolver = env -> {
    Object obj = env.getObject();

    if(obj instanceof Wallet)
      return env.getSchema().getObjectType("Wallet");
    else if(obj instanceof TopUpTransaction)
      return env.getSchema().getObjectType("TopUpTransaction");
    else
      return env.getSchema().getObjectType("TransferTransaction");
  };
}
