package com.wallet.app.transactions;

import com.wallet.app.*;
import com.wallet.app.wallets.*;

import javax.persistence.*;
import java.math.*;

@Entity
public class Transaction extends Model {
  @OneToOne(fetch = FetchType.EAGER)
  @JoinColumn
  private Wallet from;

  @OneToOne(fetch = FetchType.EAGER)
  @JoinColumn(nullable = false)
  private Wallet to;

  @Column
  private BigDecimal amount;

  public Wallet getFrom() {
    return from;
  }

  public void setFrom(Wallet arg) {
    this.from = arg;
  }

  public Transaction withFrom(Wallet arg) {
    setFrom(arg);
    return this;
  }

  public Wallet getTo() {
    return to;
  }

  public void setTo(Wallet arg) {
    this.to = arg;
  }

  public Transaction withTo(Wallet arg) {
    setTo(arg);
    return this;
  }

  public String getAmount() {
    return amount.toString();
  }

  public BigDecimal getRawAmount() {
    return amount;
  }

  public void setAmount(BigDecimal arg) {
    this.amount = arg;
  }

  public Transaction withAmount(BigDecimal arg) {
    setAmount(arg);
    return this;
  }
}
