package com.wallet.app.util;

import java.util.*;

public class ListOpts {
  private static final String STR_MAX = "max";
  private static final String STR_OFFSET = "offset";

  private Optional<Integer> offset = Optional.empty();
  private Optional<Integer> max = Optional.empty();

  public static ListOpts from(Map<String, Object> opts) {
    ListOpts listOpts = new ListOpts();

    if(Objects.isNull(opts))
      return new ListOpts();

    if(opts.containsKey(STR_MAX))
      listOpts.max = Optional.of((Integer) opts.get(STR_MAX));

    if(opts.containsKey(STR_OFFSET))
      listOpts.offset = Optional.of((Integer) opts.get(STR_OFFSET));

    return listOpts;
  }

  public Optional<Integer> getOffset() {
    return offset;
  }


  public Optional<Integer> getMax() {
    return max;
  }

}
