package com.wallet.app;

import com.wallet.app.jpa.*;
import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.context.annotation.*;
import org.springframework.web.servlet.config.annotation.*;

import java.io.*;
import java.util.*;

@SpringBootApplication(scanBasePackages={"com.wallet.app"})
public class Server {
  @Bean
  public WebMvcConfigurer configure () {
    return new WebMvcConfigurerAdapter() {
      @Override
      public void addCorsMappings (CorsRegistry registry) {
        registry.addMapping("/server")
          .allowedOrigins("*")
          .allowedMethods("GET", "POST", "OPTIONS");
      }
    };
  }

  public static void main(String[] args) {
    try {
      Properties props = new Properties();
      props.load(new FileReader("conf/config.properties"));

      Config config = new Config(props);

      JPAUtil.init(config);

      SpringApplication.run(Server.class, args);

      System.out.println("Server is running...");
    }
    catch(IOException ex) {
      System.out.println(ex.getMessage());
    }
  }
}
